﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poligonos
{
    /// <summary>
    /// La clase Cuadrado contiene las propiedades Lado y Area
    /// </summary>
    public class Cuadrado : IArea
    {
        private double lado;

        /// <summary>
        /// Esta propiedad obtiene el lado del cuadrado
        /// </summary>
        public double Lado
        {
            get => lado;
            set
            {
                if (value < 0)
                {
                    throw new ArgumentOutOfRangeException();
                }
                lado = value;
            }
        }
        /// <summary>
        /// Esta propiedad calcula el area del cuadrado
        /// 
        /// </summary>
        public double Area { get => Lado * Lado; }
    }
}
