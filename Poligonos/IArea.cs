﻿namespace Poligonos
{
/// <summary>
/// Esta es la interfaz de Area
/// </summary>
    public interface IArea
    {
        /// <summary>
        /// Consigue el valor de Area
        /// Es de tipo doble, es conveniente hacer los tests Assert.AreEquals
        /// </summary>
        double Area { get; }
    }
}